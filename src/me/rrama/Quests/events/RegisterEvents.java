package me.rrama.Quests.events;

import me.rrama.Quests.Quests;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

public class RegisterEvents {
    
    public static void Register () {
        reg(new BreakAndPlace());
        reg(new JoinAndQuit1());
        reg(new Snowball());
    }
    
    public static void reg(Listener ll) {
        Bukkit.getServer().getPluginManager().registerEvents(ll, Quests.This);
    }
    
}
