package me.rrama.Quests.events;

import me.rrama.Quests.commands.StartAndStopQuests;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinAndQuit1 implements Listener {
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player P = event.getPlayer();
        String PN = P.getName();
        if (StartAndStopQuests.Questing.keySet().contains(PN)) {
            StartAndStopQuests.Questing.remove(PN);
        }
        StartAndStopQuests.Hostname.remove(PN);
        P.setGameMode(GameMode.SURVIVAL);
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerLogin(PlayerLoginEvent event) {
        if (event.getResult() == PlayerLoginEvent.Result.ALLOWED) {
            StartAndStopQuests.Hostname.put(event.getPlayer().getName(), event.getHostname());
        }
    }
    
}
