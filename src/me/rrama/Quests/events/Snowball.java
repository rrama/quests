package me.rrama.Quests.events;

import me.rrama.Quests.Quests;
import me.rrama.Quests.commands.StartAndStopQuests;
import me.rrama.RramaGaming.GamingMap;
import me.rrama.RramaGaming.GamingMaps;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class Snowball implements Listener {
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Player P = event.getPlayer();
            String PN = P.getName();
            GamingMap GM = GamingMaps.GamingMaps.get(StartAndStopQuests.Questing.get(PN));
            if (GM == null) return;
            if (GM.getMetaData().get("Quests-Objective").equals("Snowball") && P.getItemInHand().getTypeId() == 332) {
                Quests.CompleteQuest(P);
            }
        }
    }
}
