package me.rrama.Quests.commands;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import me.rrama.Quests.Quests;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Intro implements CommandExecutor {
    
    public static ArrayList<String> Intro = new ArrayList<>();
    
    public static void ReadIntroFromFile(final File FileIntro) {
        try {
            Scanner s = new Scanner(FileIntro);
            s.reset();
            while (s.hasNextLine()) {
                Intro.add(s.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((Quests.This.TagB + "Intro.txt not found."));
        }
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("QuestsIntro") || commandLable.equalsIgnoreCase("ZIntro")) {
            sender.sendMessage(ChatColor.YELLOW + "--- Quests Intro ---");
            for (String s : Intro) {
                sender.sendMessage(ChatColor.YELLOW + s);
            }
            return true;
        } else return false;
    }
}