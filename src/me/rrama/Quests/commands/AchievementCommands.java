package me.rrama.Quests.commands;

import java.util.ArrayList;
import me.rrama.Quests.Achievements;
import me.rrama.Quests.Achievements.Achievement;
import me.rrama.Quests.Quests;
import me.rrama.RramaGaming.ListToString;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AchievementCommands implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("QuestsAchievements")) {
            if (args.length == 0) {
                if (sender instanceof Player) {
                    final String PN = ((Player)sender).getName();
                    ArrayList<String> Havers = new ArrayList<>();
                    for (Achievement A : Achievement.values()) {
                        if (Achievements.PlayerHasAchievement(PN, A)) {
                            Havers.add(A.name());
                        }
                    }
                    for (String A : Quests.This.Maps) {
                        if (Achievements.PlayerHasAchievement(PN, A)) {
                            Havers.add(A);
                        }
                    }
                    if (Havers.isEmpty()) {
                        sender.sendMessage(ChatColor.YELLOW + "You have no Quests achievements.");
                    } else {
                        final String All = ListToString.ListToString(Havers, ", ");
                        sender.sendMessage(ChatColor.YELLOW + "You have the Quests achievements: " + All + ".");
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "Consoles do not get achievements, other than being the best :)");
                }
                return true;
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("All")) {
                    ArrayList<String> Allers = new ArrayList<>();
                    for (Achievement A : Achievement.values()) {
                        Allers.add(A.name());
                    }
                    for (String A : Quests.This.Maps) {
                        Allers.add(A);
                    }
                    final String All = ListToString.ListToString(Allers, ", ");
                    sender.sendMessage(ChatColor.YELLOW + "Avalible Quests achievements: " + All + ".");
                } else {
                    Player P = Bukkit.getPlayer(args[0]);
                    if (P == null) {
                        sender.sendMessage(ChatColor.YELLOW + "Could not find the player spesicied.");
                    } else {
                        final String PN = P.getName();
                        ArrayList<String> Havers = new ArrayList<>();
                        for (Achievement A : Achievement.values()) {
                            if (Achievements.PlayerHasAchievement(PN, A)) {
                                Havers.add(A.name());
                            }
                        }
                        if (Havers.isEmpty()) {
                            sender.sendMessage(ChatColor.YELLOW + PN + " has no Quests achievements.");
                        } else {
                            final String All = ListToString.ListToString(Havers, ", ");
                            sender.sendMessage(ChatColor.YELLOW + PN + " has the Quests achievements: " + All + ".");
                        }
                    }
                }
                return true;
            } else return false;
        } else return false;
    }
}
