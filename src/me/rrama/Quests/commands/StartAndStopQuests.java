package me.rrama.Quests.commands;

import java.util.HashMap;
import me.rrama.Quests.Achievements;
import me.rrama.Quests.Achievements.Achievement;
import me.rrama.Quests.Quests;
import me.rrama.RramaGaming.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class StartAndStopQuests implements CommandExecutor {
    
    public static HashMap<String, String> Questing = new HashMap<>();
    public static HashMap<String, String> Hostname = new HashMap<>();
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("Quest")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.YELLOW + "Quests are for Players only.");
                return true;
            }
            Player P = (Player)sender;
            String PN = P.getName();
            Gamer G = RramaGaming.getGamer(PN);
            if (Questing.keySet().contains(PN)) {
                QuitQuests(P, PN);
            } else {
                JoinQuests(P, PN, G);
            }
            return true;
        } else return false;
    }
    
    public static void JoinQuests(final Player P, final String PN, final Gamer G) {
        if (RramaGaming.R == RoundState.InGame) {
            P.sendMessage(ChatColor.YELLOW + "You can not quest at this time adventurer.");
            return;
        }
        final String CurrentQuest = Quests.getGamersCurrentQuest(PN);
        if (CurrentQuest.equals("Finished")) {
            P.sendMessage(ChatColor.YELLOW + "You have already completed every quest.");
            return;
        }
        for (Gamer G2 : RramaGaming.getGamers()) {
            if (G2.getGamePlaying().equals("Quests")) {
                P.sendMessage(ChatColor.YELLOW + "Someone else is already questing. Play a regular game with them.");
                return;
            }
        }
        G.setGamePlaying("Quests");
        P.setGameMode(GameMode.ADVENTURE);
        Questing.put(PN, CurrentQuest);
        GamingMap QM = GamingMaps.GamingMaps.get(CurrentQuest);
        QM.resetMap();
        G.sendTexturePack(GamingMaps.checkTextureURL(QM.getTexture()));
        P.teleport(QM.getSpawn());
        P.getInventory().clear();
        P.getInventory().setArmorContents(null);
        Achievements.AwardAchievement(PN, Achievement.Adventurer);
        P.sendMessage(ChatColor.GOLD + "You have embarked on the quest '" + CurrentQuest + "' young adventurer.");
        P.sendMessage(ChatColor.GOLD + "Use /Quest again to quit.");
    }
    
    public static void QuitQuests(final Player P, final String PN) {
        P.sendMessage(ChatColor.YELLOW + "Popping you back into reality, like you just joined. " + ChatColor.MAGIC + "Woo!");
        Bukkit.getPluginManager().callEvent(new PlayerQuitEvent(P, ""));
        Bukkit.getPluginManager().callEvent(new PlayerLoginEvent(P, Hostname.get(PN), P.getAddress().getAddress()));
        Bukkit.getPluginManager().callEvent(new PlayerJoinEvent(P, ""));
        P.setGameMode(GameMode.SURVIVAL);
    }
}