package me.rrama.Quests;

import java.io.File;
import me.rrama.Quests.Achievements.Achievement;
import me.rrama.Quests.commands.Intro;
import org.bukkit.Bukkit;

public class FolderSetup {
    
    public static File pluginFolder, PlayersAchievementsFolder;
    
    public static void FolderSetUp() {
        boolean repeat = false;
        final String LSep = System.getProperty("line.separator");
        final String FS = System.getProperty("file.separator");
        pluginFolder = Quests.This.getDataFolder();
        if (pluginFolder.exists() == false) {
            boolean b = pluginFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((Quests.This.TagB + "DataFolder failed to be made."));
            } else {
                Bukkit.getLogger().info((Quests.This.TagB + "Created a DataFolder for you :). rrama do good?"));
            }
            FolderSetUp();
        } else {
            
            File FileQuestMaps = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "QuestMaps.txt", Quests.This.TagB, "#Format: Quest name | Objective" + LSep
                    + "#Avalible Objectives: Snowball" + LSep
                    + "#Example: Epic_Quest | Snowball");
            if (FileQuestMaps != null) {
                QuestMaps.ReadQuestMapInfo(FileQuestMaps);
            }
            
            PlayersAchievementsFolder = new File(pluginFolder, FS + "Achievements");
            if (PlayersAchievementsFolder.exists() == false) {
                boolean b = PlayersAchievementsFolder.mkdir();
                if (b == false) {
                    Bukkit.getLogger().warning((Quests.This.TagB + "Folder 'Achievements' failed to be made."));
                } else {
                    Bukkit.getLogger().info((Quests.This.TagB + "Created an Achievements folder for you :). rrama do good?"));
                }
                repeat = true;
            } else {
                for (Achievement A : Achievement.values()) {
                    me.rrama.RramaGaming.FolderSetup.CreateFile(PlayersAchievementsFolder, A + ".txt", Quests.This.TagB, null);
                }
                for (String QM : Quests.This.Maps) {
                    me.rrama.RramaGaming.FolderSetup.CreateFile(PlayersAchievementsFolder, QM + ".txt", Quests.This.TagB, null);
                }
            }
            
            File FileIntro = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "Intro.txt", Quests.This.TagB, "No intro has been written here yet.");
            if (FileIntro != null) {
                Intro.ReadIntroFromFile(FileIntro);
            }
            
            if (repeat) {
                FolderSetUp();
            }
        }
    }
}
