package me.rrama.Quests;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import me.rrama.RramaGaming.GamingMaps;
import org.bukkit.Bukkit;

public class QuestMaps {
    
    public static void ReadQuestMapInfo(final File F) {
        try (Scanner S = new Scanner(F)) {
            while (S.hasNextLine()) {
                String LS = S.nextLine();
                if (!LS.startsWith("#")) {
                    final String[] LSA = LS.split(" \\| ");
                    Quests.This.Maps.add(LSA[0]);
                    GamingMaps.GamingMaps.get(LSA[0]).addMetaData("Quests-Objective", LSA[1]);
                }
            }
            if (Quests.This.Maps.isEmpty()) {
                Bukkit.getLogger().warning((Quests.This.TagB + "No mapz in da QuestsMaps file!!! Quests plugin die now x_x ided."));
                Bukkit.getPluginManager().disablePlugin(Quests.This);
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((Quests.This.TagB + "Cannot find da QuestsMaps file!!! Quests plugin die now x_x ided."));
            Bukkit.getPluginManager().disablePlugin(Quests.This);
        }
    }
}
