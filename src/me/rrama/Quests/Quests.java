package me.rrama.Quests;

/**
 *
 * @author rrama
 * All rights reserved to rrama.
 * No copying/stealing any part of the code (Exceptions; You are from the Bukkit team, you have written consent from rrama).
 * No copying/stealing ideas from the code (Exceptions; You are from the Bukkit team, you have written consent from rrama).
 * 
 * @Credits credit goes to rrama (author)
 */

import me.rrama.Quests.commands.*;
import me.rrama.Quests.events.RegisterEvents;
import me.rrama.RramaGaming.CookieFileException;
import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Quests extends GamingPlugin {
    
    public static GamingPlugin This;
    
    @Override
    public void onGameDisable() {
        for (String PN : StartAndStopQuests.Questing.keySet()) {
            Bukkit.getPlayerExact(PN).kickPlayer("Quests stoped whilst you were questing.");
        }
    }
    
    @Override
    public void onGameEnable() {
        This = this;
        RoundPlugin = false;
        
        FolderSetup.FolderSetUp();
        
        RegisterEvents.Register();
        getCommand("Quest").setExecutor(new StartAndStopQuests());
        getCommand("QuestsIntro").setExecutor(new Intro());
        getCommand("QuestsAchievements").setExecutor(new AchievementCommands());
    }
    
    public static String getGamersCurrentQuest(final String PN) {
        for (String S : This.Maps) {
            if (!Achievements.PlayerHasAchievement(PN, S)) return S;
        }
        return "Finished";
    }
    
    public static void CompleteQuest(final Player P) {
        String PN = P.getName();
        String DoneQuest = StartAndStopQuests.Questing.get(PN);
        Achievements.AwardAchievement(PN, DoneQuest);
        try {
            RramaGaming.getGamer(PN).addCookies(1);
        } catch (CookieFileException ex) {
            ex.sendConsoleMessage();
            ex.sendPlayerMessage();
        }
        P.sendMessage(ChatColor.GOLD + "Well done for completing " + DoneQuest + ". Use /Quest to leave.");
    }
    
}
