# README #

Please note this repository's **code has not been updated since 2012!**

This repository uses the my outdated Games API found [here](https://bitbucket.org/rrama/rramagaming).

## Content ##

In this repository is a mini-game plugin for a game (Minecraft) with;

* In-game commands.
* Handling ingame events (e.g. getting to certain quest locations).
* Contains an easy way to create new quest goals and load maps for quests.
* And more!

## Author ##
[rrama](https://bitbucket.org/rrama/) (Ben Durrans).